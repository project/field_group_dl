<?php

declare(strict_types=1);

namespace Drupal\Tests\field_group_dl\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Defines a class for testing DefinitionList functionality.
 *
 * @group field_group_dl
 */
class DefinitionListTest extends BrowserTestBase {

  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'filter',
    'options',
    'node',
    'system',
    'user',
    'field_group',
    'field_group_dl',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the definition list.
   */
  public function testDefinitionList(): void {
    $this->drupalCreateContentType([
      'type' => 'page',
    ]);
    $this->createField('node', 'page', 'string', 'foo');
    $this->createField('node', 'page', 'string', 'bar');
    $value1 = $this->randomMachineName();
    $value2 = $this->randomMachineName();
    $display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page', 'full');
    assert($display instanceof EntityViewDisplayInterface);
    $display->setComponent('foo', ['type' => 'string'])
      ->setComponent('bar', ['type' => 'string'])
      ->setThirdPartySetting('field_group', 'group_dl', [
        'children' => ['foo', 'bar'],
        'parent_name' => '',
        'region' => 'content',
        'weight' => 1,
        'format_type' => 'field_group_dl',
        'label' => 'Summary',
        'format_settings' => [
          'id' => '',
          'classes' => '',
        ],
      ])->save();
    $entity = Node::create([
      'type' => 'page',
      'title' => 'test entity',
      'foo' => $value1,
      'bar' => $value2,
    ]);
    $entity->save();
    $this->drupalLogin($this->createUser(['access content']));
    $this->drupalGet($entity->toUrl());
    $dts = array_map(function (NodeElement $element) {
      return $element->getText();
    }, $this->getSession()->getPage()->findAll('css', 'dt'));
    $this->assertEquals(['Foo', 'Bar'], $dts);
    $dds = array_map(function (NodeElement $element) {
      return $element->getText();
    }, $this->getSession()->getPage()->findAll('css', 'dd'));
    $this->assertEquals([$value1, $value2], $dds);
  }

  /**
   * Creates a test field.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle
   *   Bundle ID.
   * @param string $field_type
   *   Field type.
   * @param string $field_name
   *   Field name.
   * @param array $settings
   *   Field settings.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   Created field.
   */
  protected function createField(string $entity_type_id, string $bundle, string $field_type, string $field_name, array $settings = []): FieldConfigInterface {
    if (!FieldStorageConfig::load("$entity_type_id.$field_name")) {
      $storage = FieldStorageConfig::create([
        'entity_type' => $entity_type_id,
        'field_name' => $field_name,
        'id' => "$entity_type_id.$field_name",
        'type' => $field_type,
        'settings' => $settings,
      ]);
      $storage->save();
    }

    if ($config = FieldConfig::load("$entity_type_id.$bundle.$field_name")) {
      return $config;
    }
    $config = FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type_id,
      'bundle' => $bundle,
      'id' => "$entity_type_id.$bundle.$field_name",
      'label' => Unicode::ucfirst($field_name),
    ]);
    $config->save();
    return $config;
  }

}
