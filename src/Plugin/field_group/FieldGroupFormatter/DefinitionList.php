<?php

declare(strict_types=1);

namespace Drupal\field_group_dl\Plugin\field_group\FieldGroupFormatter;

use Drupal\field_group\FieldGroupFormatterBase;

/**
 * Defines a class for outputting a group of fields as a definition list.
 *
 * @FieldGroupFormatter(
 *   id = "field_group_dl",
 *   label = @Translation("Definition list"),
 *   description = @Translation("This fieldgroup renders the content as a definition list."),
 *   format_types = {},
 *   supported_contexts = {
 *     "view",
 *   }
 * )
 */
class DefinitionList extends FieldGroupFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function preRender(&$element, $rendering_object) {
    parent::preRender($element, $rendering_object);
    $element += [
      '#theme' => 'field_group_dl',
    ];
  }

}
